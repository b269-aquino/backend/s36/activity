// Defines when particular controllers will be used 
// Contain all the endpoints and responses that we can get from controllers

const express = require("express");
//creates a router instance that functions as a middleware and routing system
const router = express.Router();

const taskController = require("../controllers/taskController");

// Route to get all the task
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));

});

// // Route to create task
router.post("/", (req,res) => {
	taskController.createTask(req.body).then(
		resultFromController => res.send(resultFromController));
})

// Route to delete task
router.delete("/:id", (req,res) => {
	taskController.deleteTask(req.params.id).then(
		resultFromController => res.send(resultFromController));
})



// [SECTION] ACTIVITY
router.get("/:id", (req, res) => {
	taskController.getOneTasks(req.params.id).then(resultFromController => res.send(resultFromController));

});


module.exports = router;